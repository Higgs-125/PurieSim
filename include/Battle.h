#ifndef BATTLE_h
#define BATTLE_h
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TMath.h"
#include "TH1D.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TLorentzVector.h"
#include "TH3F.h"
#include <TRandom3.h>
#include <TMinuit.h>
#include <TApplication.h>
#include "TEnv.h"
#include <TComplex.h>
#include <TGraph.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include "TTree.h"
#include "Map.h"
#include "Strategy.h"

using namespace std;

class Battle
{
 public:

 bool isOutput;
 bool isOutputMap;

 Map *myMap[17];
 Map *myCloneMap[17];
 Strategy* myStrategy;
 TString PositionStatus[17];
 bool isPass[17];
 bool isGenerating[17];
 bool isOccupied[17];
 bool isBlockArea[17];
 bool isShrineArea[17];
 bool isYardArea[17];
 bool isTowerArea[17];
 bool isYardTowerOccupied[5];

 bool InitBlockArea[17];
 bool InitShrineArea[17];
 bool InitYardArea[17];
 bool InitTowerArea[17];

 int TowerCount;
 int YardCount;
 int LinkWithTower[17];

 int BirdLife;

 TString Shrine[4];
 TString Yard[2];
 TString Tower[2];
 TString Block;
 TString Block1;
 TString Block2;
 TString Block3;
 TString Block4;

 vector<int> Avail;
 vector<int> YardTowerAvail;
 vector<int> GeneratingArea;
 vector<int> PassingArea;
 vector<int> BlockArea;
 vector<int> ShrineArea;
 vector<int> YardArea;
 vector<int> TowerArea;
 vector<Map *> TotalMap;

 Battle();
 virtual ~Battle();
 virtual void GenerateBlock();
 virtual void BlockClass(int BlockType);
 virtual int SelectPosition(int Number);
 virtual int WhichOccupied();
 virtual void GenerateSecondBlock(int iTower);
 virtual void ReadInitMap(vector<Map *> TotalMaps);
 virtual int StartBattle();
 virtual void UpdateStatus();
 virtual double GetPosition(int i);
 virtual void Output();
 virtual void OutputInitMap();
 virtual void CloneMap();
 virtual void InitStatus();
 virtual void RefreshBirdLife();
 virtual void End();
// virtual int BattleResult();

};
#endif
