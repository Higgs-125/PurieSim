#ifndef STRATEGY_h
#define STRATEGY_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TMath.h"
#include "TH1D.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TLorentzVector.h"
#include "TH3F.h"
#include <TRandom3.h>
#include <TMinuit.h>
#include <TApplication.h>
#include "TEnv.h"
#include <TComplex.h>
#include <TGraph.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include "TTree.h"
#include "Map.h"

using namespace std;

class Strategy
{
 public:

 Map* myMap[17];

 vector<bool> isBlock;
 vector<bool> isShrine;
 vector<bool> isYard;
 vector<bool> isTower;
 vector<bool> isAvail;
 vector<bool> isGenerating;
 vector<bool> isPass;
 vector<bool> isNull;

 int BlockCount;
 int ShrineCount;
 int YardCount;
 int TowerCount;
 int AvailCount;
 int GeneratingCount;
 int PassCount;

 int position;
 int Nposition;
 int NNposition;
 int NNNposition;
 int NNNNposition;
 int NNNNNposition;

 int NextPosition;

 bool isOutput;

 bool isOnlyNotTower;
 bool isNotEnterBlock;
 bool isFirstPassYard;
 bool isMustTwoYard;

 bool Result;

 Strategy();
 virtual ~Strategy();
 virtual bool isEnterMap(int myposition, vector<Map *> myMaps);
 virtual bool isEnterYard();
 virtual bool isEnterBlock();
 virtual bool isEnterTower();
 virtual bool isEnterShrine();
 virtual void OnlyNotTower();
 virtual void NotEnterBlock();
 virtual void FirstPassYard();
 virtual void MustTwoYard();
};
#endif
