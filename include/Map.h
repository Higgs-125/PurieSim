#ifndef MAP_h
#define MAP_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TMath.h"
#include "TH1D.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TLorentzVector.h"
#include "TH3F.h"
#include <TRandom3.h>
#include <TMinuit.h>
#include <TApplication.h>
#include "TEnv.h"
#include <TComplex.h>
#include <TGraph.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include "TTree.h"

class Map
{
 public:

 TString Type;
 TString TypeName;
 bool isBlock;
 bool isTower;
 bool isYard;
 bool isShrine;
 bool isNull;
 bool isPass;

 TString BlockType;
 TString TowerType;
 TString YardType;
 TString ShrineType;

 int WaitingTime;
 int Position;

 bool isLiving;
 bool isGenerating;

 int YardCount;

 Map();
 virtual ~Map();
 virtual void CheckType();
 virtual void InputYardCount(int myYardCount);
 virtual TString GetType();
 virtual TString GetBigType();
 virtual void UpdateStatus();
 virtual void PutIn(int myPosition, const char* myType, const char* TypeName);
 virtual void Generating();
 virtual void FinishGenerating();
 virtual void FinishTowerGenerating();
 virtual void HavePass();
 virtual int PassTime();
};
#endif
